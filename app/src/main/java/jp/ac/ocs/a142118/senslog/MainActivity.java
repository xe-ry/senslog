package jp.ac.ocs.a142118.senslog;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener, CompoundButton.OnCheckedChangeListener{

    private SensorManager mSensorManager;
    private List<Sensor> sensors;
    private Sensor mAcceleration;
    private Sensor mStepDetectorSensor;
    private Sensor mStepCounterSensor;

    private static int SHAKE_THRESHOLD = 600; //シェイクの閾値
    private static int WALK_DETECT_FREQ_THRESHOLD = 300; //頻度の閾値

    //一時保存用の変数
    private long lastUpdate;
    private long lastShaked;
    private float[] accell = new float[3];
    private int steps;

    // 活動時間
    private long actTime;
    private long lastAct;
    private Chronometer chronometer;
    private boolean changed;
    private long startedTime = 0;
    private long stoppedTime = 0;

    private TextView[] mSensor = new TextView[12];
    private TextView stepTxt;
    private TextView distTxt;
    protected Switch bgSW;

    // 閾値設定用ピッカー
    NumberPicker pkrShake;
    NumberPicker pkrFreq;
    Button updthr;
    Button defBtn;
    Button resetBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        stepTxt = (TextView)findViewById(R.id.stepTxt);
        distTxt = (TextView)findViewById(R.id.distTxt);
        bgSW = (Switch)findViewById(R.id.switch1);
        mStepDetectorSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        mStepCounterSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        steps = 0;
        lastUpdate = 0L;
        lastShaked = 0L;
        bgSW.setOnCheckedChangeListener(this);

        findViews();
        initViews();
    }

    @Override
    protected void onResume(){
        super.onResume();
//        Sensor acceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        mSensorManager.registerListener(this, acceleration, SensorManager.SENSOR_DELAY_UI);

        /*List<Sensor> */
        sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        //センサの登録と取得頻度の設定
        for (Sensor sensor : sensors) {
            if(sensor.getType() == Sensor.TYPE_ACCELEROMETER){
                mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
            }else{
                mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
            }
        }
        //その他のセンサの登録(特に意味は無い)
        mSensorManager.registerListener(this, mStepCounterSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(new SensorEventListener() { //リスナ登録もしちゃう
            private int mCounter = 0;

            @Override
            public void onSensorChanged(SensorEvent event) {
                Log.i("TYPE_STEP_DETECTOR", "" + mCounter++);
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        }, mStepDetectorSensor, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSensorManager != null) { //センサ登録解除
            mSensorManager.unregisterListener(this);
            mSensorManager.unregisterListener(this, mStepCounterSensor);
            mSensorManager.unregisterListener(this, mStepDetectorSensor);
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        float[] values = event.values;
        long timestamp = event.timestamp;

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//            mSensor[0].setText(String.valueOf(values[0]));
//            mSensor[1].setText(String.valueOf(values[1]));
//            mSensor[2].setText(String.valueOf(values[2]));

            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                float[] last = new float[3];
                last[0] = accell[0];
                last[1] = accell[1];
                last[2] = accell[2];
                lastUpdate = curTime;

//                accell = event.values;
                accell[0] = event.values[0];
                accell[1] = event.values[1];
                accell[2] = event.values[2];
                float speed = Math.abs(accell[0] + accell[1] + accell[2] - last[0] - last[1] - last[2]) / diffTime * 10000;
//                System.out.println(speed);

                // 活動時間カウントアップの停止
                if(((curTime - actTime) > 1000) && !changed){
                    stoppedTime = SystemClock.elapsedRealtime();
                    chronometer.stop();
                    changed = true;
//                    System.out.println(lastAct);
                }


                // shakeの検出
                if (speed > SHAKE_THRESHOLD) {
//                    Log.d("sensor", "shake detected w/ speed: " + speed);
//                    Toast.makeText(this, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
                    if((curTime - lastShaked) > WALK_DETECT_FREQ_THRESHOLD){
                        lastShaked = curTime;
                        actTime = curTime;

                        // 活動時間カウントアップの開始
                        if(changed){
                            startedTime = SystemClock.elapsedRealtime();
                            chronometer.setBase(SystemClock.elapsedRealtime());
                            chronometer.setBase(/*SystemClock.elapsedRealtime() - */(startedTime - stoppedTime));
                            changed = false;
                            chronometer.start();
                            System.out.println(SystemClock.elapsedRealtime());
                            System.out.println(chronometer.getBase());
                        }


                        steps++;
//                        String stp = ("歩数:" + steps + "歩");
                        stepTxt.setText(String.valueOf(steps));
                        String dst = ("距離:" + (int)(steps * 0.65) + "m");
                        distTxt.setText(dst);
                    }
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b){
            startService(new Intent(MainActivity.this, recSvc.class));
        }else{
            stopService(new Intent(MainActivity.this, recSvc.class));
        }
    }
    private void findViews(){
        pkrShake = (NumberPicker)findViewById(R.id.pkrshake);
        pkrFreq = (NumberPicker)findViewById(R.id.pkrfreq);
        updthr = (Button)findViewById(R.id.updthr);
        defBtn = (Button)findViewById(R.id.btndef);
        resetBtn = (Button)findViewById(R.id.resetbtn);
        chronometer = (Chronometer)findViewById(R.id.chronometer);
    }

    private void initViews(){
        pkrShake.setMaxValue(1000);
        pkrShake.setMinValue(100);
        pkrFreq.setMaxValue(500);
        pkrFreq.setMinValue(100);

        pkrShake.setValue(600);
        pkrFreq.setValue(300);

        updthr.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                SHAKE_THRESHOLD = pkrShake.getValue();
                WALK_DETECT_FREQ_THRESHOLD = pkrFreq.getValue();
            }
        });
        defBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                SHAKE_THRESHOLD = 600;
                WALK_DETECT_FREQ_THRESHOLD = 300;
                pkrShake.setValue(600);
                pkrFreq.setValue(300);
            }
        });
        resetBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                steps = 0;
                stepTxt.setText("0");
                chronometer.setBase(SystemClock.elapsedRealtime());

            }
        });

    }
}
